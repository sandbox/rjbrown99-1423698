<?php

/*
 * Load files with base classes of the contained classes.
 */

/**
 * Implementation of hook_views_plugins().
 */
function apachesolr_views_views_plugins() {
  return array(
    'module' => 'apachesolr_views',
    'query' => array(
      'apachesolr_views_query' => array(
        'title' => t('Apache Solr Query'),
        'help' => t('Query that allows you to search with Apache Solr.'),
        'handler' => 'apachesolr_views_query',
        'parent' => 'views_query',
      ),
    ),
  );

}

/**
 * Implementation of hook_views_data().
 */
function apachesolr_views_views_data() {
  foreach (module_invoke_all('apachesolr_entities') as $base_table => $definition) {
    $name = $definition['name'];
    $base_field = $definition['base_field'];
    $apachesolr_base_table = 'apachesolr_' . $base_table;
    $data[$apachesolr_base_table]['table']['group'] = t('Apache Solr');

    $data[$apachesolr_base_table]['table']['base'] = array(
      'query class' => 'apachesolr_views_query',
      'title' => t('Apache Solr @name', array('@name' => $name)),
      'help' => t('Searches the site with the Apache Solr search engine for @name', array('@name' => $name)),
      'field' => $base_field,
      'entity type' => 'node',
    );
    $data[$apachesolr_base_table]['entity_id'] = array(
      'title' => t('Node: Nid'),
      'help' => t('The node ID of the node.'),
      'field' => array(
        'handler' => 'apachesolr_views_handler_field_node',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'apachesolr_views_handler_sort',
      ),
    );
    $data[$apachesolr_base_table]['label'] = array(
      'title' => t('Node: Title'),
      'help' => t('The title of the node.'),
      'field' => array(
        'handler' => 'apachesolr_views_handler_field_node',
      ),
      'argument' => array(
        'handler' => 'apachesolr_views_handler_argument',
      ),
      'sort' => array(
        'handler' => 'apachesolr_views_handler_sort',
      ),
    );
    $data[$apachesolr_base_table]['created'] = array(
      'title' => t('Node: Post date'),
      'help' => t('The date the node was posted.'),
      'field' => array(
        'handler' => 'apachesolr_views_handler_field_date',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
      ),
    );
    $data[$apachesolr_base_table]['changed'] = array(
      'title' => t('Node: Updated date'),
      'help' => t('The date the node was last updated.'),
      'field' => array(
        'handler' => 'apachesolr_views_handler_field_date',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
      ),
    );
    $data[$apachesolr_base_table]['bundle'] = array(
      'title' => t('Node: Type'),
      'help' => t('The type of a node (for example, "blog entry", "forum post", "story", etc).'),
      'field' => array(
        'handler' => 'views_handler_field_node_type',
      ),
    );
  }

  return $data;
}
