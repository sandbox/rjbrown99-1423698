<?php
/**
 * @file
 * Contains the apachesolr 'node' field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a node.
 */
class apachesolr_views_handler_field_node extends views_handler_field_node {

  function get_value($values, $field = NULL) {
    $alias = isset($field) ? $this->aliases[$field] : $this->field_alias;
    // Manually account for nid and entity_id mapping. This is not
    // technically correct, but it saves us from having to make lots
    // of other upstream changes.
    $alias = ($alias == 'nid') ? $alias = 'entity_id' : $alias;
    if (isset($values->node->{$alias})) {
      return $values->node->{$alias};
    }
  }
}
