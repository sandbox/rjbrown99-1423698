<?php
/**
 * @file
 * Contains the apachesolr 'date' field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a node's
 * date fields.
 */
class apachesolr_views_handler_field_date extends views_handler_field_date {

  function get_value($values, $field = NULL) {
    $alias = isset($field) ? $this->aliases[$field] : $this->field_alias;
    if (isset($values->node->{$alias})) {
      return $values->node->{$alias};
    }
  }
}
